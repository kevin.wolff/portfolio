// Fonction "navbar"

// Variables navbar
const navbar = document.querySelector('.navbar'); // Div navbar
const scrollBar = document.querySelector('.scroll-bar'); // Div scrollbar
const propos = document.querySelector('.propos-wrapper'); // Div propos

// Variables link
const navLink = document.querySelectorAll('.nav-link');

// Fonction pour attribuer class "sticky" a navbar selon scrollTop
function stickyNavbar(device) {
  document.addEventListener('scroll', () => {
    if (window.pageYOffset > device.step) {
      navbar.classList.add('sticky');
      scrollBar.classList.add('sticky');
      propos.classList.add('sticked');
      for (let i = 0; i < navLink.length; i++) {
        navLink[i].classList.add('sticky');
      }
    } else {
      navbar.classList.remove('sticky');
      scrollBar.classList.remove('sticky');
      propos.classList.remove('sticked');
      for (let i = 0; i < navLink.length; i++) {
        navLink[i].classList.remove('sticky');
      }
    }
  });
}

// EXPORT

export { stickyNavbar };
