// Fonction "animations-launcher"

// Variables link
const navLink = document.querySelectorAll('.nav-link');

// Variables animation a propos
const proposDownload = document.querySelector('.download');
const proposBox = document.querySelectorAll('.propos-box');

// Variables animation competences
const competencesBtn = document.querySelectorAll('.box-button');
const competencesForm = document.getElementById('competences-form'); // Div forme competences
const competencesParallax = document.getElementById('competences-parallax'); // Div parallax competences

// Variables animation realisations
const project = document.querySelectorAll('.realisations-left-box');

// Variables animation contact
const gitlab = document.getElementById('gitlab'); // SVG gitlab
// const linkedin = document.getElementById('linkedin'); // SVG linkedIn
const email = document.getElementById('email'); // SVG email
const phone = document.getElementById('phone'); // SVG phone

// Fonction qui attribue/enleve class selon scrollTop
function animationLauncher(device) {
  window.addEventListener('scroll', () => {
    // Lien 0 actif
    if (window.pageYOffset < device.step1) {
      for (let i = 0; i < navLink.length; i++) {
        if (navLink[i] == navLink[0]) {
          navLink[i].classList.add('active');
        } else {
          navLink[i].classList.remove('active');
        }
      }
    } else if (window.pageYOffset > device.step1 && window.pageYOffset < device.step2) {
      // Lien 1 actif
      for (let i = 0; i < navLink.length; i++) {
        if (navLink[i] == navLink[1]) {
          navLink[i].classList.add('active');
        } else {
          navLink[i].classList.remove('active');
        }
      }
      // Animation actif
      proposDownload.classList.add('animate');
      for (let i = 0; i < proposBox.length; i++) {
        proposBox[i].classList.add('animate');
      }
    } else if (window.pageYOffset > device.step2 && window.pageYOffset < device.step3) {
      // Lien 2 actif
      for (let i = 0; i < navLink.length; i++) {
        if (navLink[i] == navLink[2]) {
          navLink[i].classList.add('active');
        } else {
          navLink[i].classList.remove('active');
        }
      }
      // Animation actif
      for (let i = 0; i < competencesBtn.length; i++) {
        competencesBtn[i].classList.add('animate');
      }
      competencesForm.classList.add('animate');
      competencesParallax.classList.add('animate');
    } else if (window.pageYOffset > device.step3 && window.pageYOffset < device.step4) {
      // Lien 3 actif
      for (let i = 0; i < navLink.length; i++) {
        if (navLink[i] == navLink[3]) {
          navLink[i].classList.add('active');
        } else {
          navLink[i].classList.remove('active');
        }
      }
      // Animation actif
      for (let i = 0; i < project.length; i++) {
        project[i].classList.add('animate');
      }
    } else if (window.pageYOffset > device.step4) {
      // Lien 4 actif
      for (let i = 0; i < navLink.length; i++) {
        if (navLink[i] == navLink[4]) {
          navLink[i].classList.add('active');
        } else {
          navLink[i].classList.remove('active');
        }
      }
      // Animation actif
      gitlab.classList.add('active');
      // linkedin.classList.add('active');
      email.classList.add('active');
      phone.classList.add('active');
    }
  });
}

// EXPORT

export { animationLauncher };
