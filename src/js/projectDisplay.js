// Module réalisations display

// Variable animation realisations
const project1 = document.getElementById('project1'); // Div projet n°1
const project2 = document.getElementById('project2'); // Div projet n°2
const project3 = document.getElementById('project3'); // Div projet n°3
const project4 = document.getElementById('project4'); // Div projet n°4
const project5 = document.getElementById('project5'); // Div projet n°5

const project = document.querySelectorAll('.realisations-left-box'); // Div all projet

// Variables display
const display = document.querySelector('.realisation-right-display'); // Div display img
const displayZoom = document.getElementById('display-zoom'); // Link zoom image
const urlLink = document.getElementById('url-link'); // Url link
const urlSvg = document.getElementById('url'); // Url svg
const gitLink = document.getElementById('git-link'); // Git link
const gitSvg = document.getElementById('git'); // Git svg

project1.addEventListener('click', () => {
  // Display projet
  display.style.background = 'url("./assets/img/brame.jpg") no-repeat center / cover';
  displayZoom.setAttribute('href', './assets/img/brame.jpg');
  // Set url link
  urlLink.setAttribute('href', '#');
  urlSvg.classList.add('disable');
  // Set git link
  gitLink.setAttribute('href', 'https://gitlab.com/kevin.wolff/brame');
  gitSvg.classList.remove('disable');
  // Display ou hide autres projets
  for (let i = 0; i < project.length; i++) {
    if (project[i] == project[0]) {
      project[i].classList.add('active');
    } else {
      project[i].classList.remove('active');
    }
  }
});

project2.addEventListener('click', () => {
  // Display projet
  display.style.background = 'url("./assets/img/studio.jpg") no-repeat center / cover';
  displayZoom.setAttribute('href', './assets/img/studio.jpg');
  // Set url link
  urlLink.setAttribute('href', '#');
  urlSvg.classList.add('disable');
  // Set git link
  gitLink.setAttribute('href', 'https://gitlab.com/kevin.wolff/tp7-kgb-database');
  // Display ou hide autres projets
  for (let i = 0; i < project.length; i++) {
    if (project[i] == project[1]) {
      project[i].classList.add('active');
    } else {
      project[i].classList.remove('active');
    }
  }
});

project3.addEventListener('click', () => {
  // Display projet
  display.style.background = 'url("./assets/img/mobilite.jpg") no-repeat center / cover';
  displayZoom.setAttribute('href', './assets/img/mobilite.jpg');
  // Set url link
  urlLink.setAttribute('href', '#');
  urlSvg.classList.add('disable');
  // Set git link
  gitLink.setAttribute('href', 'https://gitlab.com/kevin.wolff/tp6-metro');
  gitSvg.classList.remove('disable');
  // Display ou hide autres projets
  for (let i = 0; i < project.length; i++) {
    if (project[i] == project[2]) {
      project[i].classList.add('active');
    } else {
      project[i].classList.remove('active');
    }
  }
});

project4.addEventListener('click', () => {
  // Display projet
  display.style.background = 'url("./assets/img/review.jpg") no-repeat center / cover';
  displayZoom.setAttribute('href', './assets/img/review.jpg');
  // Set url link
  urlLink.setAttribute('href', '#');
  urlSvg.classList.add('disable');
  // Set git link
  gitLink.setAttribute('href', 'https://gitlab.com/kevin.wolff/tp5-allocinelike');
  gitSvg.classList.remove('disable');
  // Display ou hide autres projets
  for (let i = 0; i < project.length; i++) {
    if (project[i] == project[3]) {
      project[i].classList.add('active');
    } else {
      project[i].classList.remove('active');
    }
  }
});

project5.addEventListener('click', () => {
  // Display projet
  display.style.background = 'url("./assets/img/simplogotchi.jpg") no-repeat center / cover';
  displayZoom.setAttribute('href', './assets/img/simplogotchi.jpg');
  // Set url link
  urlLink.setAttribute('href', '#');
  urlSvg.classList.add('disable');
  // Set git link
  gitLink.setAttribute('href', 'https://gitlab.com/kevin.wolff/tp4-tamagochi');
  gitSvg.classList.remove('disable');
  // Display ou hide autres projets
  for (let i = 0; i < project.length; i++) {
    if (project[i] == project[4]) {
      project[i].classList.add('active');
    } else {
      project[i].classList.remove('active');
    }
  }
});
