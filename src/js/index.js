// IMPORT

import '../sass/style.scss';

import { scrollProgress } from './scrollProgress'; // Fonction scroll progress navbar
import { animationLauncher } from './animationsLauncher'; // Fonction animation launcher window
import { stickyNavbar } from './stickyNavbar'; // Fonction sticky navbar
import { proposDisplay } from './proposDisplay'; // Script propos display
import { certificationDisplay } from './certificationDisplay'; // Script certification display
import { projectDisplay } from './projectDisplay'; // Script project display

// Fonction module "scroll-progress" navbar
window.onscroll = function () {
  scrollProgress();
};

// Step pour chaque ecran - fonction module "animations-launcher"
const desktop = {
  step1: 85,
  step2: 900,
  step3: 1760,
  step4: 2450,
};
const tablet = {
  step1: 430,
  step2: 1500,
  step3: 2420,
  step4: 3190,
};
const mobile = {
  step1: 345,
  step2: 1460,
  step3: 2550,
  step4: 3220,
};

// Step pour chaque ecran - fonction module "sticky-navbar"
const desktopNav = {
  step: 441,
};
const tabletNav = {
  step: 376,
};
const mobileNav = {
  step: 311,
};

// Execute fonction module "animationLauncher" + "stickyNavbar" au redimensionnement de la page
function redimensionnement(e) {
  if ('matchMedia' in window) {
    if (window.matchMedia('(min-width:768px)').matches) {
      animationLauncher(desktop);
      stickyNavbar(desktopNav);
    } else if (window.matchMedia('(min-width:480px) and (max-width:768px)').matches) {
      animationLauncher(tablet);
      stickyNavbar(tabletNav);
    } else if (window.matchMedia('(max-width:480px').matches) {
      // animationLauncher(mobile);
      // stickyNavbar(mobileNav);
    }
  }
}

// On lie l'evenement redimensionnement a la fonction
window.addEventListener('resize', redimensionnement, false);

// Execution de la fonction au demarrage pour avoir un retour initial
redimensionnement();

// CONSOLE LOG SCROLL Y - NO NEED //

// document.addEventListener('scroll', () => {
//   console.log(scrollY);
// });