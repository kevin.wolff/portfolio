// Module certification display

// Variables certification
const certifBtn1 = document.getElementById('btn-competences-1'); // Bouton certif agil
const certifBtn2 = document.getElementById('btn-competences-2'); // Bouton certif Opquast
const certifBtn3 = document.getElementById('btn-competences-3'); // Bouton format Simplon

const competencesRight = document.querySelector('.competences-right'); // Compétences par défaut

const certificationInfo = document.querySelectorAll('.certfication-infos'); // All div certification

const closeAgile = document.getElementById('close-agile'); // Bouton close info certif agil
const closeOpquast = document.getElementById('close-opquast'); // Bouton close info certif Opquast
const closeSimplon = document.getElementById('close-simplon'); // Bouton close info format Simplon

certifBtn1.addEventListener('click', () => {
  // Hide competences par defaut
  competencesRight.classList.add('hide');
  // Display info certif selectionné
  for (let i = 0; i < certificationInfo.length; i++) {
    if (certificationInfo[i] == certificationInfo[0]) {
      certificationInfo[i].classList.add('show');
    } else {
      certificationInfo[i].classList.remove('show');
    }
  }
});

certifBtn2.addEventListener('click', () => {
  // Hide competences par defaut
  competencesRight.classList.add('hide');
  // Display info certif selectionné
  for (let i = 0; i < certificationInfo.length; i++) {
    if (certificationInfo[i] == certificationInfo[1]) {
      certificationInfo[i].classList.add('show');
    } else {
      certificationInfo[i].classList.remove('show');
    }
  }
});

certifBtn3.addEventListener('click', () => {
  // Hide competences par defaut
  competencesRight.classList.add('hide');
  // Display info certif selectionné
  for (let i = 0; i < certificationInfo.length; i++) {
    if (certificationInfo[i] == certificationInfo[2]) {
      certificationInfo[i].classList.add('show');
    } else {
      certificationInfo[i].classList.remove('show');
    }
  }
});

closeAgile.addEventListener('click', () => {
  // Display competences par defaut
  competencesRight.classList.remove('hide');
  // Hide info certif
  for (let i = 0; i < certificationInfo.length; i++) {
    certificationInfo[i].classList.remove('show');
  }
});

closeOpquast.addEventListener('click', () => {
  // Display competences par defaut
  competencesRight.classList.remove('hide');
  // Hide info certif
  for (let i = 0; i < certificationInfo.length; i++) {
    certificationInfo[i].classList.remove('show');
  }
});

closeSimplon.addEventListener('click', () => {
  // Display competences par defaut
  competencesRight.classList.remove('hide');
  // Hide info certif
  for (let i = 0; i < certificationInfo.length; i++) {
    certificationInfo[i].classList.remove('show');
  }
});
