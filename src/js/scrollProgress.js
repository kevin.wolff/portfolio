// Fonction "scroll-progress" navbar

// Variable scrollbar
const scrollBar = document.querySelector('.scroll-bar');

// Fonction progress de la navbar selon le scrollTop
function scrollProgress() {
  const winwscroll = document.body.scrollTop || document.documentElement.scrollTop; // Scroll value
  // Overview height
  const height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  const scrolled = (winwscroll / height) * 100;
  scrollBar.style.width = `${scrolled}%`;
}

// EXPORT

export { scrollProgress };
