// Module propos display

// Display une box de l'app
const allBoxUnclick = document.querySelectorAll('.box-unclick'); // All div box-unclick
const loisir = document.getElementById('loisir'); // Box décalée

for (const boxUnclick of allBoxUnclick) {
  boxUnclick.addEventListener('click', () => {
    // Selecteur propos-box et box-click les plus proches
    const parentProposBox = boxUnclick.closest('.propos-box');
    closestBoxClick = parentProposBox.querySelector('.box-click');

    // Display la box de l'app séléctionné
    boxUnclick.classList.add('click');
    parentProposBox.classList.add('click');
    closestBoxClick.classList.add('click');
    loisir.classList.add('hide'); // Hide la box décalée
  });
}

// Hide une box de l'app
const allBoxCloseBtn = document.querySelectorAll('.box-close-btn'); // All boutons box-close-btn

for (const boxCloseBtn of allBoxCloseBtn) {
  boxCloseBtn.addEventListener('click', () => {
    // Selecteur propos-box, box-click et box-unclick les plus proches
    const parentPropoxBox = boxCloseBtn.closest('.propos-box');
    const closestBoxClick = parentPropoxBox.querySelector('.box-click');
    const closestBoxUnclick = parentPropoxBox.querySelector('.box-unclick');

    // Hide la box de l'app séléctionné
    parentPropoxBox.classList.remove('click');
    closestBoxClick.classList.remove('click');
    closestBoxUnclick.classList.remove('click');
    loisir.classList.remove('hide'); // Display la box décalée
  });
}
